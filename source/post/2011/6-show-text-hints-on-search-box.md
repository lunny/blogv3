```ini
title = 在输入框/搜索框中显示提示文字的JS效果
date = 2011-04-26 06:14:43
tags = js,提示文字,效果,特效,输入框
```

我敢说，做前端的十个里八个做过这个。今天把我的做法记录一下哈，直接上代码：

```Javascript
function trim(str){
    return str.replace(/^\s*(.*?)[\s\n]*$/g,'$1')
}
var ic = false
function a(){return document.getElementById("a")}

function cha() {return trim(a().value).length > 0}

function ifblur() {
    ic = cha()
    if (!ic) {
        a().value = "我是提示文字"
         a().style.color = "gray"
     }
 }

function iffocus() {
     if (!ic) {
         a().value = ""
         a().style.color = "black"
         ic = true
     }
}
```
