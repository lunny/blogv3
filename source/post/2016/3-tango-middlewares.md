```ini
title = Go语言Web框架Tango中的中间件应用级别
date = 2016-05-12 14:32:00
tags = go,golang,tango,web,framework,middleware,design
```

Tango在创建之初就支持了全局中间件的支持，经过最近的改进，同时支持了Group级别和Route级别的中间件。下面我们就一起来看下这三种级别的中间件：

比如我们自定义了一个新的中间件：

```Go
func MyMiddleware() tango.HandlerFunc {
	return func(ctx *tango.Context) {
		ctx.Info("I'm a middleware")
        ctx.Next()
	}
}
```

# 全局中间件

全局中间件在任何一个请求调用时均会进行调用。用法如下：

```Go
t := tango.Classic()
t.Use(MyMiddleware())
t.Get("/", func() string {return "global"})
```

# Group中间件

Group中间件在Group下匹配的路由被调用时会被调用。用法如下：

```Go
t := tango.Classic()
t.Group("/group", func(g *tango.Group) {
    g.Use(MyMiddleware())
    g.Get("/", func() string {return "group"})
})
```

# Route中间件

Route中间件在该Route被调用时会被调用。如果有多个，会按照先后顺序调用。用法如下：

```Go
t := tango.Classic()
t.Get("/route", func() string {return "route"}, MyMiddleware())
```

# 中间件优先级

* 全局中间件被先调用，Group中间件次之，最后是Route中间件
* 相同级别的中间件，先加入的被先调用