```ini
title = Go语言库net/smtp发送邮件故障一则
date = 2016-01-07 16:00:00
tags = go,golang,smtp,x509,roots,email
```

在通过net/smtp包发送邮件时遇到一个奇怪的错误，在mac和windows上运行都能够成功发送邮件，但是将程序跨平台编译后上传到debian linux执行报：
```
x509: failed to load system roots and no roots provided
```
的错误，看样子是在认证邮件服务器证书的根证书时没有找到本地的根证书库。最后通过运行
```
#apt-get install ca-certificates
```
之后，邮件成功发送。
