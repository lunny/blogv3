```ini
title = Gop - 编译和管理在GOPATH之外的Go工程
date = 2017-08-15 22:04:00
tags = go,gop,gopath,build,dependency,management
```

# 安装

```Go
go get github.com/lunny/gop
```

# 起子

自开始使用Go进行开发之后，工程一直都保存在GOPATH之下，Go1.5支持 `vendor` 机制之后开始使用 `govendor` 来管理依赖项。其实一直都有需求要将 Go 的工程放在 GOPATH 之外，因为在一个大的项目中，各种语言写的内容放在一个 git 工程的子文件夹中，但一直没有很好的工具来解决依赖的问题。

几个月之前，这个问题已经严重影响到工作了，终于不能忍受了。于是动手写了 `Gop` 的首个版本，最近又升级到了0.3版本。最新版本的 `Gop` 工程目录结构如下：


```
<project root>
├── gop.yml
├── bin
├── doc
└── src
    ├── main
    │   └── main.go
    ├── models
    │   └── models.go
    ├── routes
    │   └── routes.go
    └── vendor
        └── github.com
            ├── go-xorm
            │   ├── builder
            │   ├── core
            │   └── xorm
            └── lunny
                ├── log
                └── tango
```

通过以上的目录结构可以看到，其实gop是兼容GOPATH的。只要把 `<project root>` 设置到GOPATH中，即使没有安装`gop`命令，通过`go`命令也可以编译，但是这时对 `vendor` 管理也是不太方便。但如果使用 `gop` 来管理项目，则问题迎刃而解。

其中 `gop.yml` 的结构如下：

```yml
targets:
- name: myproject1
  dir: main
  assets:
  - templates
  - public
  - config.ini
  - key.pem
  - cert.pem
- name: myproject2
  dir: web
  assets:
  - templates
  - public
  - config.ini
```

除了可以放到 `GOPATH` 之外，Gop 工程还具有以下特性：

* 多编译目标支持

默认的编译目标是 `src/main` 目录，编译名字是 `src` 的父目录的名字，当然你也可以通过 `gop.yml` 来自定义。如果输入`gop build` 命令，默认会编译第一个编译目标。如果指定了编译目标，如 `gop build myproject2` 那么将会编译指定的目标。默认编译好的可执行文件会放在 `src/main/` 目录下，也可以通过 `gop build -o ./bin/my.exe` 来进行指定。除了 `<target>` 参数外，其它的参数和 `go build` 的参数相同。

* 依赖管理

一般只需要执行 `gop ensure -g` 即可将所有的依赖项全部下载且拷贝到 `vendor` 下。如果依赖项需要更新，也可以使用 `gop ensure -u` 将所有的依赖项更新到最新的版本。不再需要的依赖项，可以通过 `gop rm <package1> <package2>` 删除，通过 `gop status` 可以查看默认编译目标的依赖项的安装情况。

* 编译并运行某个目标

`gop run` 可以让你编译并运行某个目标，通过 `-w`，在源码修改之后还可以自动重新编译并运行。

* 发布管理

`gop release` 编译，打包资源并拷贝到 `bin` 目录下。