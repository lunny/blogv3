```ini
title = Tango v0.4版本发布，带来统一高性能的新路由
date = 2015-04-06 15:23:00
tags = go,web,tango,framework,pluggable,router
```

# 起子

自Tango发布之后大概有3个月了，受到了很多人的关注，也提了很多的改进意见。我自己也通过不断
的用他开发，从而发现问题，不断改进。昨天我发布了0.4.0版本，很明显，最近版本号升得比较快。

从0.3到0.4，最重大的改变是路由方面的。路由基本上完全重写，目前路由变得更统一，更灵活，性能上也有不少提升。来看看新的路由如何使用。

# 新路由

目前支持4种路由，静态路由，命名路由，Catch-All路由，正则路由。下面分别说说4种路由：

## 静态路由

静态路由是最普遍的，默认的net/http目前只支持静态路由，比如：

```Go
tango.Get("/", new(Action))
tango.Get("/public/bootstrap.min.css", new(Action))
```

## 命名路由

在0.4之前的版本中，命名路由的形式一般是`:name`，在0.4版本兼容以前的模式，并且新增了`(:name)`的形式。这个新的设计将允许使用字符或者数字作为分隔符。比如，新旧版本都支持这种形式：

```Go
tango.Get("/:name", new(Action))
tango.Get("/:name1-:name2", new(Action))
tango.Get("/:name1/:name2", new(Action))
```

而新版本增加了对下列形式的支持：

```Go
tango.Get("/(:name1)abc(:name2)", new(Action))
tango.Get("/(:name1)123(:name2)", new(Action))
```

## Catch-All路由

Catch-All路由是0.4新增的一种路由形式。Catch-All路由和命名路由很相似，他们的的唯一区别在于
命名路由不能匹配/，而Catch-All路由可以匹配/。比如：

```Go
tango.Get("/*name", new(Action))
tango.Get("/*name/123", new(Action))
```

比如当访问 `/name1/name2/123` 时，第二个路由会匹配出 `*name = name1/name2`。当访问`/name1/name2`时，则第一个路由会匹配出 `*name = name1/name2`。

获取Catch-All路由请通过 `Params.Get("*name")`。

## 正则路由

**注意：0.4版本的正则路由和老版本的正则路由不兼容**

为了统一表达形式，0.4版本抛弃了以前版本中完全使用Go的正则的方式来进行匹配的方式，如：

0.3版本以前：

```Go
tango.Get("/(.*)", new(Action))
```

而获取参数通过 Params.Get(":0")来获取。

0.3版本到0.4版本之间：

```Go
tango.Get("/(?P<name>.*)", new(Action))
```

而获取参数通过 Params.Get(":name")来获取，同时也可以使用`Params.Get(":0")`来获取。

0.4版本不兼容前面的语法，再次提醒注意，而采用如下方法，匿名的正则已不被允许，`Params.Get(":0")`也不再受支持：

```Go
tango.Get("/(:name.*)", new(Action))
tango.Get("/(:name[0-9]+)", new(Action))
```

而获取参数通过 Params.Get(":name")来获取，也可以通过Params[0].Value来获取，Params目前变成了一个Slice。

# 路由优先级

1. 静态路由和其它路由都匹配时，静态路由优先，跟添加的顺序无关；
2. 其它路由之间根据添加的顺序，先添加的优先。

例如:
```Go
t := tango.Classic()
t.Get("/:name", new(Others))
t.Get("/admin", new(Admin))
t.Run()
```
以上代码，当请求 `/admin` 时, `Admin` 的 `Get` 方法将被执行。

```Go
t := tango.Classic()
t.Get("/:name", new(Admin))
t.Get("/*name", new(Others))
t.Run()
```
以上代码，当请求 `/admin` 时, `Admin` 的 `Get` 方法将被执行；当请求 `/admin/ui`, `Others` 的 `Get` 方法将被执行。

```Go
t := tango.Classic()
t.Get("/*name", new(Admin))
t.Get("/:name", new(Others))
t.Run()
```

以上代码， `Others` 的 `Get` 方法将永远不会被执行, 因为所有匹配的请求均会调用 `Admin` 的 `Get` 方法。

# 注意事项

关于新路由也有几点需要注意的事项：

* 命名和正则等可以混合使用，如：`/:name1-(:name2[0-9]+)`，但是要注意，必须要有分隔符，没有分隔符，在新增路由时会panic，比如如下形式就不受支持：`/:name1:name2`

* 命名理论上是可以相同的，并且程序不会报错，但是不建议这样使用。通过Params.Get(":name")只会返回第一个，可通过自己遍历Params来获取多个相同的名字。比如：`/:name-:name`，这种规则是允许的，但是不推荐。

# 收尾

新的路由形式上更加统一，相信可以满足大家的绝大部分需求。希望大家多提宝贵意见，可加QQ群：369240307，或者在 [github.com/lunny/tango](https://github.com/lunny/tango) 以及 [git.oschina.net/lunny/tango](http://git.oschina.net/lunny/tango) 中提出issue。
