```ini
title = 买了一个新的数码相机 佳能A710IS
date = 2007-04-21 14:22:49
tags = 数码相机,佳能,A710IS
```

性能参数：710w象素，6倍光学变焦，1/2.5英寸传感器，2.5英寸液晶屏，光学防抖。

[![A710IS](/static/media/images/2007/04/a710is.jpg)](/static/media/images/2007/04/a710is.jpg)

用它拍了几张照片，放一张上来瞧瞧！

[![A710 IS拍的照片](/static/media/images/2007/04/00001.jpg)](/static/media/images/2007/04/00001.jpg)
