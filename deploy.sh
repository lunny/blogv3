pugo build --theme="theme/pure" --dest="$GOPATH/src/github.com/lunny/lunny.github.io"
cd $GOPATH/src/github.com/lunny/lunny.github.io
git add --all
git commit -m "updated"
git push origin master
notify "blog updated successfully"
